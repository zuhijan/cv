import {ReactComponent as LindkenInLogo} from "./linkedin.svg";
import {ReactComponent as MailLogo} from "./mail.svg";
import {ReactComponent as TelegramLogo} from "./telegram.svg";

export {LindkenInLogo, MailLogo, TelegramLogo};
