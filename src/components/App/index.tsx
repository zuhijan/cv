import React from 'react';
import './App.module.scss';
import {LindkenInLogo, MailLogo, TelegramLogo} from "../../icons";
import styles from './App.module.scss'



function App() {
  return (
    <div className={styles.app}>
      <a
        href="https://www.linkedin.com/in/egorshkirya/"
        target="_blank"
        rel="noopener noreferrer"
      >
      <LindkenInLogo className={styles.icon}/>
      </a>
      <a
        href="mailto:egor.shkiria@gmail.com"
        target="_blank"
        rel="noopener noreferrer"
      >
      <MailLogo className={styles.icon}/>
      </a>
      <a
        href="https://t.me/egorshkirya"
        target="_blank"
        rel="noopener noreferrer"
      >
      <TelegramLogo className={styles.icon}/>
      </a>
    </div>
  );
}

export default App;

